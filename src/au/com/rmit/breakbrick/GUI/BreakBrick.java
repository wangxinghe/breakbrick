/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.rmit.breakbrick.GUI;

import au.com.philology.common.JavaTheme;
import javax.swing.SwingUtilities;

/**
 *
 * @author ricolwang
 */
public class BreakBrick
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                JavaTheme.setLookAndFeel(JavaTheme.LOOKANDFEEL_MOTIF);
                new FrameMain().setVisible(true);
            }
        });
    }

}
