/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.rmit.breakbrick.common;

/**
 *
 * @author ricolwang
 */
public class Common
{
    public static final float SPEED_EXPLODE_PARTICLE = 300;
    
    public static final int LAYER_TEXT = 1;
    public static final int LAYER_BRICK = 0;
    
    public static final int CATEGORY_WALL = 1;
    public static final int CATEGORY_BRICK = 2;
    public static final int CATEGORY_PANEL = 3;
    public static final int CATEGORY_BALL = 4;
    
    public static final int SCORE_BRICK = 5;
    
    public static final int SPEED_PANEL = 500;
    public static final int SPEED_BALL_X = 400;
    public static final int SPEED_BALL_Y = 400;
}
