/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.rmit.breakbrick.scene;

import au.com.rmit.Game2dEngine.action.AlphaByAction;
import au.com.rmit.Game2dEngine.action.AlphaToAction;
import au.com.rmit.Game2dEngine.geometry.SpecialRectangleShape;
import au.com.rmit.Game2dEngine.sprite.LabelSprite;
import au.com.rmit.Game2dEngine.sprite.Sprite;
import au.com.rmit.breakbrick.common.Common;
import au.com.rmit.breakbrick.sprites.Ball;
import au.com.rmit.breakbrick.sprites.Brick;
import au.com.rmit.breakbrick.sprites.MovingObject;
import au.com.rmit.breakbrick.sprites.Panel;
import au.com.rmit.breakbrick.sprites.Score;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;

/**
 *
 * @author ricolwang
 */
public class BreakBrickScene extends WallScene
{

    public boolean bGameRunning;
    public LabelSprite lblMyLife;
    public LabelSprite lblScore;

    int mylife = 3;
    int score = 0;

    Panel thePanel;
    Ball theBall;

    public BreakBrickScene()
    {
        this.enableCollisionDetect();
    }

    private void addLabels()
    {
        int tmpWidth = 150;
        int tmpHeight = 20;

        if (lblMyLife == null)
        {
            lblMyLife = new LabelSprite(0, 0, "My Life: " + this.mylife, null);

            lblMyLife.setWidth(tmpWidth);

            lblMyLife.setHeight(tmpHeight);

            lblMyLife.setRed(
                255);
            lblMyLife.bTextFrame = false;
            lblMyLife.setLayer(Common.LAYER_TEXT);

            addSprite(lblMyLife);
        }

        if (lblScore == null)
        {

            lblScore = new LabelSprite(0, 0, "Score: " + this.score, null);

            lblScore.setWidth(tmpWidth);

            lblScore.setHeight(tmpHeight);

            lblScore.setRed(
                255);
            lblScore.bTextFrame = false;
            lblScore.setLayer(Common.LAYER_TEXT);

            addSprite(lblScore);
        }
        this.adjustLabelPos();
    }

    public void adjustLabelPos()
    {
        int tmpY = 20;
        int tmpMarginRight = 140;
        int tmpHeight = 20;
        int tmpGap = 1;
        if (lblMyLife != null)
        {
            lblMyLife.setX(this.getWidth() - tmpMarginRight);
            lblMyLife.setY(tmpY);
        }
        if (lblScore != null)
        {
            lblScore.setX(this.getWidth() - tmpMarginRight);
            lblScore.setY(tmpY + (tmpHeight + tmpGap) * 1);
        }
    }

    public void breakABrick(Brick aBrick)
    {
        if (!this.bGameRunning)
            return;

        Score aScore = new Score("+" + Common.SCORE_BRICK);
        aScore.setWidth(50);
        aScore.setHeight(15);
        aScore.setCentreX(aBrick.getCentreX());
        aScore.setCentreY(aBrick.getCentreY());
        this.addSprite(aScore);

        this.score += Common.SCORE_BRICK;

        this.updateLabels();
    }

    public void lostALife()
    {
        this.mylife--;
        this.updateLabels();
    }

    public void updateLabels()
    {
        if (this.lblScore != null)
        {
            this.lblScore.setText("Score: " + this.score);
        }
        if (this.lblMyLife != null)
        {
            this.lblMyLife.setText("My Life: " + this.mylife);
        }
    }

    @Override
    public void setSize(Dimension d)
    {
        super.setSize(d); //To change body of generated methods, choose Tools | Templates.

        this.adjustLabelPos();
    }

    public void gameStart()
    {
        this.buildWalls();

        mylife = 3;
        score = 0;
        this.updateLabels();

        LabelSprite aLabel = new LabelSprite("Game Start", new Font("TimesRoman", Font.PLAIN, 30));
        aLabel.setWidth(150);
        aLabel.setHeight(30);
        aLabel.textPosY = 25;
        aLabel.setVelocityY(-50);
        aLabel.bTextFrame = true;
        aLabel.bDeadIfNoActions = true;
        aLabel.setCentreX(this.getWidth() / 2);
        aLabel.setCentreY(this.getHeight() / 2);

        AlphaToAction aAction = new AlphaToAction(aLabel);
        aAction.alphaTo(0, 1.5f);
        aLabel.addAction(aAction);

        this.addSprite(aLabel);

        this.thePanel = new Panel();
        this.thePanel.setCentreX(this.getWidth() / 2.0f);
        this.thePanel.setCentreY(this.theWallBottom.getY() - this.thePanel.getHeight() / 2 - 5);
        this.thePanel.setTheShape(new SpecialRectangleShape(thePanel.getX(), thePanel.getY(), thePanel.getWidth(), thePanel.getHeight()));
        addSprite(this.thePanel);

        theBall = new Ball();
        theBall.setCentreX(thePanel.getCentreX());
        theBall.setY(thePanel.getY() - theBall.getHeight() - 10);
        addSprite(theBall);

        //add top bricks
        for (int i = 1; i <= 5; i++)
        {
            for (int j = 1; j <= 10; j++)
            {
                Brick aBrick = new Brick();
                double width = aBrick.getWidth();
                double heigth = aBrick.getHeight();
                double gap = 10;
                aBrick.setCentreX((j + 1) * (width + gap));
                aBrick.setCentreY((i + 2) * (heigth + gap));
                aBrick.setTheShape(new SpecialRectangleShape(aBrick.getX(), aBrick.getY(), aBrick.getWidth(), aBrick.getHeight()));

                addSprite(aBrick);
            }
        }

        addLabels();

        bGameRunning = true;
    }

    public void gameEnd()
    {
        for (Sprite aSprite : this.getAllSprites())
        {
            if (aSprite instanceof MovingObject)
            {
                AlphaByAction aAction = new AlphaByAction();
                aAction.alphaBy(-1, 1);
                aSprite.addAction(aAction);
                aSprite.bDeadIfNoActions = true;
            }
        }

        mylife = 3;
        score = 0;
        this.updateLabels();

        LabelSprite aLabel = new LabelSprite("Game End", new Font("TimesRoman", Font.PLAIN, 30));
        aLabel.setWidth(150);
        aLabel.setHeight(30);
        aLabel.textPosY = 25;
        aLabel.setVelocityY(-50);
        aLabel.bTextFrame = true;
        aLabel.bDeadIfNoActions = true;
        aLabel.setCentreX(this.getWidth() / 2);
        aLabel.setCentreY(this.getHeight() / 2);

        AlphaToAction aAlphaAction = new AlphaToAction(aLabel);
        aAlphaAction.alphaTo(0, 1.5f);
        aLabel.addAction(aAlphaAction);

        this.addSprite(aLabel);
        this.bGameRunning = false;
    }

    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            if (this.bGameRunning)
                this.gameEnd();
            else
                this.gameStart();
        } else if (this.bGameRunning)
        {
            if (e.getKeyCode() == KeyEvent.VK_LEFT)
            {
                thePanel.movingLeft();
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
            {
                thePanel.movingRight();
            }
        }
    }

    public void keyReleased(KeyEvent e)
    {
        if (this.bGameRunning)
        {
        }
    }

}
