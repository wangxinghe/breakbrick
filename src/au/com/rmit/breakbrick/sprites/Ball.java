/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.rmit.breakbrick.sprites;

import au.com.rmit.Game2dEngine.action.AlphaToAction;
import au.com.rmit.Game2dEngine.geometry.CircleShape;
import au.com.rmit.Game2dEngine.geometry.SpecialRectangleShape;
import au.com.rmit.Game2dEngine.geometry.Shape;
import au.com.rmit.Game2dEngine.geometry.Shape.CircleRectangleCollideDirection;
import au.com.rmit.Game2dEngine.sprite.Sprite;
import au.com.rmit.breakbrick.common.Common;
import au.com.rmit.breakbrick.scene.BreakBrickScene;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author ricolwang
 */
public class Ball extends MovingObject
{

    public Ball()
    {
        super();

        this.setWidth(20);
        this.setHeight(20);

        this.bCustomDrawing = true;
        this.setVelocityX(Common.SPEED_BALL_X);
        this.setVelocityY(-Common.SPEED_BALL_Y);

        this.bCollisionDetect = true;
        this.setCollisionCategory(Common.CATEGORY_BALL);
        this.addTargetCollisionCategory(Common.CATEGORY_WALL);
        this.addTargetCollisionCategory(Common.CATEGORY_BRICK);
        this.addTargetCollisionCategory(Common.CATEGORY_PANEL);
    }

    @Override
    public void onCustomDraw(Graphics2D theGraphics2D)
    {
        super.onCustomDraw(theGraphics2D); //To change body of generated methods, choose Tools | Templates.

        theGraphics2D.setColor(Color.red);
        theGraphics2D.fillArc(0, 0, (int) this.getWidth(), (int) this.getHeight(), 0, 360);
    }

    @Override
    public void onCollideWith(Sprite target)
    {
        super.onCollideWith(target); //To change body of generated methods, choose Tools | Templates.

        if (target instanceof Wall)
        {
            Wall aWall = (Wall) target;
            if (aWall.wallType == Wall.WALLTYPE.LEFT)
            {
                this.setVelocityX(-this.getVelocityX());
            } else if (aWall.wallType == Wall.WALLTYPE.RIGHT)
            {
                this.setVelocityX(-this.getVelocityX());
            } else if (aWall.wallType == Wall.WALLTYPE.TOP)
            {
                this.setVelocityY(-this.getVelocityY());
            } else if (aWall.wallType == Wall.WALLTYPE.BOTTOM)
            {
                this.setVelocityY(-this.getVelocityY());
            }
        } else if (target instanceof Panel)
        {
            this.setVelocityY(-this.getVelocityY());
        } else if (target instanceof Brick)
        {
            Brick theBrick = (Brick) target;
            CircleShape theCircleShape = (CircleShape) this.getTheShape();
            SpecialRectangleShape theRectange = (SpecialRectangleShape) theBrick.getTheShape();

            Shape.CircleRectangleCollideDirection theDirection = Shape.CircleCollideWithRectangleFromDirection(theCircleShape, theRectange);
            if (theDirection == CircleRectangleCollideDirection.FROM_TOP)
            {
                this.setVelocityY(-this.getVelocityY());
            } else if (theDirection == CircleRectangleCollideDirection.FROM_LEFT)
            {
                this.setVelocityX(-this.getVelocityX());
            } else if (theDirection == CircleRectangleCollideDirection.FROM_BOTTOM)
            {
                this.setVelocityY(-this.getVelocityY());
            } else if (theDirection == CircleRectangleCollideDirection.FROM_RIGHT)
            {
                this.setVelocityX(-this.getVelocityX());
            } else if (theDirection == CircleRectangleCollideDirection.FROM_TOP_LEFT)
            {
                this.setVelocityX(-this.getVelocityX());
                this.setVelocityY(-this.getVelocityY());
            } else if (theDirection == CircleRectangleCollideDirection.FROM_BOTTOM_LEFT)
            {
                this.setVelocityX(-this.getVelocityX());
                this.setVelocityY(-this.getVelocityY());
            } else if (theDirection == CircleRectangleCollideDirection.FROM_BOTTOM_RIGHT)
            {
                this.setVelocityX(-this.getVelocityX());
                this.setVelocityY(-this.getVelocityY());
            } else if (theDirection == CircleRectangleCollideDirection.FROM_TOP_RIGHT)
            {
                this.setVelocityX(-this.getVelocityX());
                this.setVelocityY(-this.getVelocityY());
            }
            
            AlphaToAction aAction = new AlphaToAction(theBrick);
            aAction.alphaTo(0, 0.5f);
            theBrick.bDeadIfNoActions = true;
            theBrick.bCollisionDetect = false;
            theBrick.addAction(aAction);
            
            ((BreakBrickScene)this.theScene).breakABrick(theBrick);
        }
    }
}
