/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.rmit.breakbrick.sprites;

import au.com.rmit.breakbrick.common.Common;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author ricolwang
 */
public class Brick extends MovingObject
{

    public Brick()
    {
        super();

        this.setWidth(60);
        this.setHeight(20);

        this.bCustomDrawing = true;

        this.bCollisionDetect = true;
        this.setCollisionCategory(Common.CATEGORY_BRICK);
    }

    @Override
    public void onCustomDraw(Graphics2D theGraphics2D)
    {
        super.onCustomDraw(theGraphics2D); //To change body of generated methods, choose Tools | Templates.

        theGraphics2D.setColor(Color.yellow);
        theGraphics2D.fillRect(0, 0, (int) this.getWidth(), (int) this.getHeight());
    }

}
