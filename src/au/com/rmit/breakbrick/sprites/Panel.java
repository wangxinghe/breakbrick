/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.rmit.breakbrick.sprites;

import au.com.rmit.Game2dEngine.physics.collision.PhysicsCollisionProcess;
import au.com.rmit.breakbrick.common.Common;
import au.com.rmit.breakbrick.scene.WallScene;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author ricolwang
 */
public class Panel extends MovingObject
{

    public Panel()
    {
        super();

        this.setWidth(80);
        this.setHeight(20);
        this.bCustomDrawing = true;
        this.setCollisionCategory(Common.CATEGORY_PANEL);
        this.addTargetCollisionCategory(Common.CATEGORY_BALL);
    }

    @Override
    public void didUpdateState()
    {
        super.didUpdateState(); //To change body of generated methods, choose Tools | Templates.

        this.checkWall();
    }

    @Override
    public void onCustomDraw(Graphics2D theGraphics2D)
    {
        super.onCustomDraw(theGraphics2D); //To change body of generated methods, choose Tools | Templates.

        theGraphics2D.setColor(Color.green);
        theGraphics2D.fillRect(0, 0, (int) this.getWidth(), (int) this.getHeight());
    }

    public void movingLeft()
    {
        this.setVelocityX(-Common.SPEED_PANEL);
    }

    public void movingRight()
    {
        this.setVelocityX(Common.SPEED_PANEL);
    }

    void checkWall()
    {
        if (this.theScene == null)
            return;

        boolean bHitWall = false;

        if (PhysicsCollisionProcess.isCollide(this, (((WallScene) this.theScene).theWallTop))
            || PhysicsCollisionProcess.isCollide(this, (((WallScene) this.theScene).theWallBottom))
            || PhysicsCollisionProcess.isCollide(this, (((WallScene) this.theScene).theWallLeft))
            || PhysicsCollisionProcess.isCollide(this, (((WallScene) this.theScene).theWallRight)))
            bHitWall = true;

        if (bHitWall)
            this.restorePosition();
    }
}
